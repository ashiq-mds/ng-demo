import { Component, OnInit } from '@angular/core';
import { OrderStateService } from 'src/app/services/state-services/order-state.service';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.scss'],
})
export class ViewOrderComponent implements OnInit {
  constructor(public orderState: OrderStateService) {}

  ngOnInit(): void {}
}
