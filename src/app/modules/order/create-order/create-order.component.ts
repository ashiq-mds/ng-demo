import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerStateService } from 'src/app/services/state-services/customer-state.service';
import { OrderStateService } from 'src/app/services/state-services/order-state.service';
import { ProductStateService } from 'src/app/services/state-services/product-state.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss'],
})
export class CreateOrderComponent implements OnInit {
  orderForm: FormGroup;
  constructor(
    public customerState: CustomerStateService,
    public productState: ProductStateService,
    public orderState: OrderStateService,
    private fb: FormBuilder
  ) {
    this.orderForm = this.createForm();
  }

  ngOnInit(): void {}

  onSubmit(){
    
  }

  private createForm() {
    return this.fb.group({
      customer: ['-1', [Validators.required]],
      products: this.fb.array([]),
      deliveryAddress: [null, [Validators.required]],
    });
  }
}
