import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CreateOrderComponent } from './create-order/create-order.component';
import { DetailsOrderComponent } from './details-order/details-order.component';
import { OrderRoutingModule } from './order-routing.module';
import { ViewOrderComponent } from './view-order/view-order.component';

@NgModule({
  declarations: [
    CreateOrderComponent,
    ViewOrderComponent,
    DetailsOrderComponent,
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    RouterModule,
    ReactiveFormsModule,
  ],
})
export class OrderModule {}
