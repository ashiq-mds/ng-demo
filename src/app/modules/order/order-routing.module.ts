import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateOrderComponent } from './create-order/create-order.component';
import { DetailsOrderComponent } from './details-order/details-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';

const routes: Routes = [
  { path: '', component: ViewOrderComponent },
  { path: 'new', component: CreateOrderComponent },
  { path: 'edit:/id', component: CreateOrderComponent },
  { path: 'details:/id', component: DetailsOrderComponent },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class OrderRoutingModule {}
