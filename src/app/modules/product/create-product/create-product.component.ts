import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductStateService } from 'src/app/services/state-services/product-state.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
})
export class CreateProductComponent implements OnInit {
  productForm: FormGroup;

  constructor(
    public productState: ProductStateService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.productForm = this.createForm();
  }

  ngOnInit(): void {}

  onSubmit() {
    this.productState.add(this.productForm.value);
    this.router.navigate(['/product']);
  }

  private createForm() {
    return this.fb.group({
      id: [],
      name: [null, [Validators.required]],
      description: [null, [Validators.required]],
      price: [null, [Validators.required]],
    });
  }
}
