import { Component, OnInit } from '@angular/core';
import { ProductStateService } from 'src/app/services/state-services/product-state.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss'],
})
export class ViewProductComponent implements OnInit {
  constructor(public productState: ProductStateService) {}

  ngOnInit(): void {}
}
