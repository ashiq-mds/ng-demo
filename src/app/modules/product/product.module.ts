import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CreateProductComponent } from './create-product/create-product.component';
import { DetailsProductComponent } from './details-product/details-product.component';
import { ProductRoutingModule } from './product-routing.module';
import { ViewProductComponent } from './view-product/view-product.component';

@NgModule({
  declarations: [
    CreateProductComponent,
    ViewProductComponent,
    DetailsProductComponent,
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    RouterModule,
    ReactiveFormsModule,
  ],
})
export class ProductModule {}
