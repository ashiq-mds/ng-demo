import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SortableDirective } from './directives/sortable.directive';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [SortableDirective],
  imports: [
    CommonModule
  ],
  exports:[
    SortableDirective
  ]
})
export class DemoCommonModule { }
