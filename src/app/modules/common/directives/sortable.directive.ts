import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { SortDirection, SortEvent } from '../../../models/page-result.model';

const rotate: {[key:string]:SortDirection}={'asc':'desc', 'desc':'asc', '':'asc'}

@Directive({
  selector: '[demoSort]',
  host:{
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    'class':'cursor-pointer',
    '(click)':'reverse()'
  }
})
export class SortableDirective {

  @Input() sortBy:string = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();
  @Input() type:string = 'object';
  constructor() { }

  reverse() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortBy, direction: this.direction, type: this.type });
  }
 
  setSort(sort: SortEvent) {
    if (sort.column === this.sortBy)
      this.direction = sort.direction;
  }
}
