import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ContainerComponent } from './container/container.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutComponent } from './layout.component';

@NgModule({
  declarations: [NavbarComponent, ContainerComponent, HomeComponent, LayoutComponent],
  imports: [CommonModule, RouterModule],
})
export class LayoutModule {}
