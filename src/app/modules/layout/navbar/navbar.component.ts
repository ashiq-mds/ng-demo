import { Component, OnInit } from '@angular/core';
import { CustomerStateService } from 'src/app/services/state-services/customer-state.service';
import { OrderStateService } from 'src/app/services/state-services/order-state.service';
import { ProductStateService } from 'src/app/services/state-services/product-state.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  constructor(
    public customerState: CustomerStateService,
    public productState: ProductStateService,
    public orderState: OrderStateService
  ) {}

  ngOnInit(): void {}
}
