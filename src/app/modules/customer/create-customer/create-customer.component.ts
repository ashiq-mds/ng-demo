import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { CustomerStateService } from 'src/app/services/state-services/customer-state.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss'],
})
export class CreateCustomerComponent implements OnInit {
  customerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private customerStateService: CustomerStateService,
    private router: Router
  ) {
    this.customerForm = this.createForm();
  }

  ngOnInit(): void {}

  onSubmit() {
    this.customerStateService.add(this.customerForm.value);
    this.router.navigate(['/customer']);
  }

  private createForm() {
    return this.fb.group({
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      contactNo: new FormControl(null, [Validators.required]),
      emailAddress: new FormControl(null, [Validators.required]),
      addresses: this.fb.array([this.createAddressForm()]),
    });
  }

  private createAddressForm() {
    return this.fb.group({
      city: new FormControl(null, [Validators.required]),
      street: new FormControl(null, [Validators.required]),
      zipCode: new FormControl(null, []),
      area: new FormControl(null, []),
    });
  }

  addAddress() {
    this.addresses.push(this.createAddressForm());
  }
  removeAddress(index: number) {
    this.addresses.removeAt(index);
  }

  get addresses() {
    return this.customerForm.get('addresses') as FormArray;
  }
}
