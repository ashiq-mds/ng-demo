import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NumberDirective } from 'src/app/modules/common/directives/number.directive';
import { CustomersSharedDataService } from 'src/app/services/datasource-services/customer-shared.datasource';
import { DemoCommonModule } from '../common/common.module';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { CustomerRoutingModule } from './customer-routing.module';
import { DetailsCustomerComponent } from './details-customer/details-customer.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';

@NgModule({
  declarations: [
    CreateCustomerComponent,
    ViewCustomerComponent,
    DetailsCustomerComponent,
    NumberDirective
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    DemoCommonModule
  ],
  providers:[CustomersSharedDataService]
})
export class CustomerModule {}
