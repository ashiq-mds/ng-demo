import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';
import { SortEvent } from 'src/app/models/page-result.model';
import { CustomersSharedDataService } from 'src/app/services/datasource-services/customer-shared.datasource';
import { CustomerDataSource } from 'src/app/services/datasource-services/customer.datasource';
import { CustomerStateService } from 'src/app/services/state-services/customer-state.service';
import { SortableDirective } from '../../common/directives/sortable.directive';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.scss'],
})
export class ViewCustomerComponent implements OnInit,AfterViewInit {

  @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;

  public customers: Customer[] = [];
  dataSource: CustomerDataSource;
  sortEvent: SortEvent;
  defaultSortKey: string = "name";
  defaultColumnType: string = 'object';
  constructor(public customerStateService: CustomerStateService,
    private customerSharedDataService:CustomersSharedDataService
    ) {

      this.dataSource = new CustomerDataSource(this.customerSharedDataService);

    this.customerStateService.getAll$.subscribe((res) => {
      this.customers = res.map((customer) => {
        customer.fullName = `${customer.firstName} ${customer.lastName}`;
        return customer;
      });
    });
  }
  ngAfterViewInit(): void {
   this.resetSortDirection();
  }

  ngOnInit(): void {
    this.loadCustomer();
    setTimeout(() => {
      this.headers.forEach(header => {
        header.setSort(
          { column: this.sortEvent.column, direction: this.sortEvent.direction, type: 'object' })
      })
    })
  }

  loadCustomer(){
    this.dataSource.loadCustomer();
  }

  onSort(sort:SortEvent){
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortBy !== sort.column) {
        header.direction = '';
      }
    });

    this.sortEvent.column = sort.column;
    this.sortEvent.direction = sort.direction;

    this.dataSource.sort(sort)
  }

  resetSortDirection() {
    this.sortEvent = { column: this.defaultSortKey, direction: "asc", type: this.defaultColumnType }

    this.headers.forEach(header => {
      if (header.sortBy !== this.defaultSortKey) {
        header.direction = '';
      } else {
        header.direction = 'asc';
      }
    });
  }
}
