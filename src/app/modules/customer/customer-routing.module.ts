import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { DetailsCustomerComponent } from './details-customer/details-customer.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';

const routes: Routes = [
  { path: '', component: ViewCustomerComponent },
  { path: 'new', component: CreateCustomerComponent },
  { path: 'edit:/id', component: CreateCustomerComponent },
  {
    path: 'details:/id',
    component: DetailsCustomerComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class CustomerRoutingModule {}
