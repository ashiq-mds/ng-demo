import { Address } from './adress.model';

export interface Customer {
  id?: number;
  firstName: string;
  lastName: string;
  fullName: string;
  contactNo: string;
  emailAddress: string;
  addresses: Address[];
}
