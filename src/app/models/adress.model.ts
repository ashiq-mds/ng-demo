export interface Address {
  city: string;
  street: string;
  zipCode: number;
  area: string;
}
