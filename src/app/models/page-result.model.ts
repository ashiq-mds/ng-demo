export class PagedResult {
    page: number;
    pageSize: number;
    sortColumn: string;
    sortDirection: SortDirection;
    showExpired: boolean = false;
    showInactive: boolean = false;
    defaultSort: SortEvent[] = [];
    searchTerm: string | undefined;
    searchId: number | undefined;
    columnType: string;
    showCompleted: boolean = false;
    showCancelled: boolean = false;
    constructor(page: number,
      pageSize: number,
      sortColumn: string,
      sortDirection: SortDirection,
      defaultSort: SortEvent[],
      columnType: string) {
      this.page = page;
      this.pageSize = pageSize;
      this.sortColumn = sortColumn;
      this.sortDirection = sortDirection;
      this.defaultSort = defaultSort;
      this.columnType = columnType;
    }
  }
  export type SortDirection = 'asc' | 'desc' | '';

  /**
 * Object represent column sort event.
 */
export class SortEvent {
    column: string | undefined;
    direction: SortDirection = '';
    type: string = "string";
  }