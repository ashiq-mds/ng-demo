import { Address } from './adress.model';
import { Customer } from './customer.model';
import { Product } from './product.model';

export interface Order {
  id?: number;
  customer: Customer;
  orderDate: Date;
  orderItems: Product[];
  deliveryAddress: Address;
}
