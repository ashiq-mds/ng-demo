import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Customer } from 'src/app/models/customer.model';

@Injectable()
export class CustomersSharedDataService {
  private customerSource = new BehaviorSubject<Array<Customer>>([]);

  customers = this.customerSource.asObservable();

  setCustomers(customers: Array<Customer>) {
    this.customerSource.next(customers);
  }

  clearCustomer() {
    this.customerSource.next([]);
  }
}