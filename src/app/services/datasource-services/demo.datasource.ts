import { Inject, Injectable, Optional } from "@angular/core";
import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { PagedResult, SortEvent } from "src/app/models/page-result.model";

@Injectable({
    providedIn:'root'
})
export class DemoDatasource<T> implements DataSource<T>{
    pageSizeOptions: number[] = [5, 10, 15, 20];
    minPageSize: number = 5;

    _loading$ = new BehaviorSubject<boolean>(false);
    collectionSubject = new BehaviorSubject<T[]>([]);
    totalCount = new BehaviorSubject<number>(0);
    _filter$ = new Subject<void>();
    _pagedResult: PagedResult;
    _records$ = new BehaviorSubject<T[]>([]);

    /**
     *
     */
    constructor(@Optional() @Inject('defaultSortEvent') defaultSortEvent: SortEvent[]) {
        this._pagedResult = new PagedResult(1, 5, '', '', defaultSortEvent, 'object');
    }

    get page() { return this._pagedResult.page; }
    get pageSize() { return this._pagedResult.pageSize; }

    connect(collectionViewer: CollectionViewer): Observable<T[]> {
        console.log('Connecting data source');
        return this.collectionSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.collectionSubject.complete();
        this._loading$.complete();
        this.totalCount.complete();
    }
    observableCollection(): Observable<T[]> {
        console.log("Connecting data source");
        return this.collectionSubject.asObservable();
      }

    clear() {
        this.collectionSubject = new BehaviorSubject<T[]>([]);
      }

    sort(sortEv:SortEvent){
        this.collectionSubject.subscribe(res=>{
            if(sortEv.direction==='asc'){
                res.sort((a,b)=>{
                    if(a[sortEv.column]>b[sortEv.column]){
                        return 1;
                    }
                    else if(a[sortEv.column]<b[sortEv.column]){
                        return -1;
                    }
                    else{
                        return 0
                    }
                })
            }else{
                res.sort((a,b)=>{
                    if(a[sortEv.column]>b[sortEv.column]){
                        return -1;
                    }
                    else if(a[sortEv.column]<b[sortEv.column]){
                        return 1;
                    }
                    else{
                        return 0
                    }
                })
            }
        })
    }
      

}