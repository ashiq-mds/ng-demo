import { Observable, of } from "rxjs";
import { delay, switchMap, tap } from "rxjs/operators";
import { Customer } from "src/app/models/customer.model";
import { SortEvent } from "src/app/models/page-result.model";
import { CustomersSharedDataService } from "./customer-shared.datasource";
import { DemoDatasource } from "./demo.datasource";

export class CustomerDataSource extends DemoDatasource<Customer>{
    private _dataLoaded:boolean = false;

    constructor(
        private customersSharedDataService: CustomersSharedDataService
    ) {
        super([
            { column: "CreatedOn", direction: "desc", type: 'string' }
          ]);
    }

    loadCustomer(){
        this._loading$.next(true);
        setTimeout(()=>{
            const customers = this.dummyCustomers();
            customers.subscribe(res=>{
                this._dataLoaded = true;
                res.map(cust=>{
                    cust.fullName = `${cust.firstName} ${cust.lastName}`;
                })
                this.customersSharedDataService.setCustomers(res);
                this.collectionSubject.next(res);
                this._records$.next(res);
                this.totalCount.next(res.length)
            });
            this._loading$.next(false)
        }, 3000)
    }

    get dataLoaded() { return this._dataLoaded; }

    get customerList() { return this._records$.asObservable(); }

    private dummyCustomers():Observable<Customer[]>{
        return of(
            [
                {id:1, firstName:'Reshad', lastName:'Rahman', addresses:[],fullName:'',contactNo:'12345786', emailAddress:''},
                {id:2, firstName:'Ashiq', lastName:'', addresses:[],fullName:'',contactNo:'54213456', emailAddress:''},
                {id:3, firstName:'Swapan', lastName:'Rahman', addresses:[],fullName:'',contactNo:'245678965', emailAddress:''},
                {id:4, firstName:'Test', lastName:'Rahman', addresses:[],fullName:'',contactNo:'7894568789', emailAddress:''},
            ]
        )
    }
    private _filter(): Observable<any> {

        // const { sortColumn, sortDirection, defaultSort, columnType, showExpired } = this._pagedResult;
        // const list = showExpired ? this.collectionSubject.value : this.collectionSubject.value
        //   .filter(note => note.NoteStatusId === this.OpenStatusId &&
        //     !(note.ExpiryDate && UtilityService.compareDate(new Date(), note.ExpiryDate) != -1));
         // 1. sort
         let customers = this.dummyCustomers();
    
        return of({ customers });
      }
}