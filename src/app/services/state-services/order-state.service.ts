import { Injectable } from '@angular/core';
import { Order } from 'src/app/models/order.model';
import { GenericStateService } from './generic-state.service';

@Injectable({
  providedIn: 'root',
})
export class OrderStateService extends GenericStateService<Order> {
  constructor() {
    super();
  }
}
