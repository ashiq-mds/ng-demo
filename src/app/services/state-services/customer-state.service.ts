import { Injectable } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';
import { GenericStateService } from './generic-state.service';
@Injectable({
  providedIn: 'root',
})
export class CustomerStateService extends GenericStateService<Customer> {
  constructor() {
    super();
  }
}
