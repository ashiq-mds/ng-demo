import { BehaviorSubject, Observable } from 'rxjs';

// @Injectable({
//   providedIn: 'root',
// })
export abstract class GenericStateService<TModel> {
  private _state: BehaviorSubject<TModel[]> = new BehaviorSubject<TModel[]>([]);

  constructor() {}

  public getAll$: Observable<TModel[]> = this._state.asObservable();

  add(item: TModel): void {
    this._state.next([...this._state.getValue(), item]);
  }

  count() {
    return this._state.getValue().length;
  }
}
