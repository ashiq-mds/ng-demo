import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { GenericStateService } from './generic-state.service';

@Injectable({
  providedIn: 'root',
})
export class ProductStateService extends GenericStateService<Product> {
  constructor() {
    super();
  }
}
